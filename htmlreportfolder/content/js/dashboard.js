/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7692307692307693, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)  ", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "36 /"], "isController": false}, {"data": [1.0, 500, 1500, "7 /"], "isController": false}, {"data": [0.0, 500, 1500, "Login"], "isController": true}, {"data": [1.0, 500, 1500, "44 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "Submit risk with file attachment"], "isController": true}, {"data": [1.0, 500, 1500, "61 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "Open homepage"], "isController": true}, {"data": [1.0, 500, 1500, "Open Risk management"], "isController": true}, {"data": [0.0, 500, 1500, "Submit risk"], "isController": true}, {"data": [1.0, 500, 1500, "37 /reports"], "isController": false}, {"data": [1.0, 500, 1500, "Logout"], "isController": true}, {"data": [1.0, 500, 1500, "55 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "60 /logout.php"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 14, 0, 0.0, 895.0714285714287, 12, 6093, 6028.0, 6093.0, 6093.0, 0.7414861500979821, 2.8404671031725015, 1.1161083728351253], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["36 /", 2, 0, 0.0, 6028.0, 5963, 6093, 6093.0, 6093.0, 6093.0, 0.12849341471249598, 0.6821193382589142, 0.1783724502088018], "isController": false}, {"data": ["7 /", 2, 0, 0.0, 72.5, 14, 131, 131.0, 131.0, 131.0, 0.2052334530528476, 0.2803921883016932, 0.07596042842483325], "isController": false}, {"data": ["Login", 2, 0, 0.0, 6051.5, 5984, 6119, 6119.0, 6119.0, 6119.0, 0.12832028743744384, 1.1177900038496087, 0.28126453628256126], "isController": true}, {"data": ["44 /management/index.php", 2, 0, 0.0, 24.0, 19, 29, 29.0, 29.0, 29.0, 0.21059281878487945, 1.4145091871117195, 0.08884384542487102], "isController": false}, {"data": ["Submit risk with file attachment", 2, 0, 0.0, 69.5, 51, 88, 88.0, 88.0, 88.0, 0.2099517111064455, 1.4278356602981315, 1.3269604241024564], "isController": true}, {"data": ["61 /index.php", 2, 0, 0.0, 18.5, 12, 25, 25.0, 25.0, 25.0, 0.21132713440405748, 0.28149434699915465, 0.08543890004226543], "isController": false}, {"data": ["Open homepage", 2, 0, 0.0, 72.5, 14, 131, 131.0, 131.0, 131.0, 0.20366598778004075, 0.278250700101833, 0.07538028258655805], "isController": true}, {"data": ["Open Risk management", 2, 0, 0.0, 24.0, 19, 29, 29.0, 29.0, 29.0, 0.21059281878487945, 1.4145091871117195, 0.08884384542487102], "isController": true}, {"data": ["Submit risk", 2, 0, 0.0, 6265.5, 6136, 6395, 6395.0, 6395.0, 6395.0, 0.12545477355413376, 3.3641236592021078, 1.3218694133421152], "isController": true}, {"data": ["37 /reports", 2, 0, 0.0, 23.5, 21, 26, 26.0, 26.0, 26.0, 0.21072595090085344, 0.7169621220103256, 0.1693627515541039], "isController": false}, {"data": ["Logout", 2, 0, 0.0, 48.0, 38, 58, 58.0, 58.0, 58.0, 0.21070375052675938, 0.6786142277707543, 0.25967591129372103], "isController": true}, {"data": ["55 /management/index.php", 2, 0, 0.0, 69.5, 51, 88, 88.0, 88.0, 88.0, 0.20992967355935763, 1.4276857877611002, 1.3268211399181276], "isController": false}, {"data": ["Transaction Controller", 0, 0, NaN, NaN, 9223372036854775807, -9223372036854775808, NaN, NaN, NaN, 0.0, 0.0, 0.0], "isController": true}, {"data": ["60 /logout.php", 2, 0, 0.0, 29.5, 26, 33, 33.0, 33.0, 33.0, 0.21128248468201985, 0.39904328649904924, 0.1749683076272977], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 14, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
